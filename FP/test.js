const readline = require('readline-sync')
const R = require('ramda')
const fs = require('fs')
const Maybe = require('folktale/maybe')
let transactions = JSON.parse(fs.readFileSync('./transactions.json', 'utf-8'))

const hasDueDeliveries = transactions => {
    let dueDeliveries = R.filter(transaction => transaction.delivered === false && new Date > new Date(transaction.deliveryArrivalDate), transactions)
    if (dueDeliveries.length > 0) {
        return true
    }
    return false
}

const processDueDeliveries = () => {
    let inventoryJSON = JSON.parse(fs.readFileSync('./inventory.json', 'utf-8'))
    let transactionsJSON = JSON.parse(fs.readFileSync('./transactions.json', 'utf-8'))
    let dueDeliveries = R.filter(transaction => transaction.delivered === false && new Date > new Date(transaction.deliveryArrivalDate), transactions)

    R.forEach(delivery => {
        R.forEach(deliveryItem => {
            inventoryJSON[R.findIndex(inventoryItem =>deliveryItem.id === inventoryItem.id, inventoryJSON)].stock += deliveryItem.stock
            transactionsJSON[R.findIndex(transaction => transaction.deliveryArrivalDate === delivery.deliveryArrivalDate, transactions)].delivered = true
        }, delivery.itemsBought)
    }, dueDeliveries)

    fs.writeFileSync('./inventory.json', JSON.stringify(inventoryJSON, null, 1))
    fs.writeFileSync('./transactions.json', JSON.stringify(transactionsJSON, null, 1))
}

processDueDeliveries()
let newDeliveriesArrived = false
let newDeliveries
if (hasDueDeliveries(transactions)) {
    newDeliveries = R.filter(transaction => transaction.delivered === false && new Date > new Date(transaction.deliveryArrivalDate), transactions)
    newDeliveriesArrived = true
}

let inventory = JSON.parse(fs.readFileSync('./inventory.json', 'utf-8'))
inventory = inventory.sort((itemA, itemB) => R.prop('id', itemA) > R.prop('id', itemB))
fs.writeFileSync('./inventory.json', JSON.stringify(inventory, null, 2))

const findLongestStringLength = R.pipe(
    R.map(R.toString),
    R.map(R.length),
    R.reduce(Math.max, 0)
)

const displayInventory = (inventory, isManager) => {
    const longestID = findLongestStringLength(R.pluck('id', inventory))
    const longestName = findLongestStringLength(R.pluck('name', inventory))
    const longestDescription = findLongestStringLength(R.pluck('description', inventory))
    const longestSellingPrice = findLongestStringLength(R.pluck('sellingPrice', inventory)) + 1
    const longestCostPrice = findLongestStringLength(R.pluck('costPrice', inventory)) + 1
    const longestStock = findLongestStringLength(R.pluck('stock', inventory))

    let inventoryString = ''
    inventoryString += `|${longestID > 2 ? ' '.repeat(Math.floor(longestID / 2)) : ' '}ID${longestID > 2 ? ' '.repeat(Math.ceil(longestID / 2)) : ' '}`
    inventoryString += `|${longestName > 4 ? ' '.repeat(Math.floor(longestName / 2) - 2): ' '}Name${longestName > 4 ? ' '.repeat(Math.ceil(longestName / 2) - 2) : ' '}`
    inventoryString += `|${longestDescription > 11 ? ' '.repeat(Math.floor(longestDescription / 2) - 5): ' '}Description${longestDescription > 11 ? ' '.repeat(Math.ceil(longestDescription / 2) - 6) : ' '}`
    if (isManager) {
        inventoryString += `|${longestSellingPrice > 13 ? ' '.repeat(Math.floor(longestSellingPrice / 2) - 2): ' '}Selling Price${longestSellingPrice > 15 ? ' '.repeat(Math.ceil(longestSellingPrice / 2) - 3) : ' '}`
        inventoryString += `|${longestCostPrice > 10 ? ' '.repeat(Math.floor(longestSellingPrice / 2) - 2): ' '}Cost Price${longestSellingPrice > 12 ? ' '.repeat(Math.ceil(longestSellingPrice / 2) - 3) : ' '}`
        inventoryString += `|${longestStock > 11 ? ' '.repeat(Math.floor(longestStock / 2) - 7): ' '}Store Stock${longestStock > 11 ? ' '.repeat(Math.ceil(longestStock / 2) - 6) : ' '}|`
        inventoryString += '\n' + '-'.repeat(longestID + longestName + longestDescription + (longestSellingPrice > 15 ? longestSellingPrice : 15) + (longestCostPrice > 10 ? longestCostPrice : 10) + (longestStock > 11 ? longestStock : 11) + 14) + '\n'
    }
    else {
        inventoryString += `|${longestSellingPrice > 5 ? ' '.repeat(Math.floor(longestSellingPrice / 2) - 2): ' '}Price${longestSellingPrice > 5 ? ' '.repeat(Math.ceil(longestSellingPrice / 2) - 3) : ' '}`
        inventoryString += `|${longestStock > 5 ? ' '.repeat(Math.floor(longestStock / 2) - 2): ' '}Stock${longestStock > 5 ? ' '.repeat(Math.ceil(longestStock / 2) - 3) : ' '}|`
        inventoryString += '\n' + '-'.repeat((longestID === 1 ? 2 : longestID) + longestName + longestDescription + (longestSellingPrice > 7 ? longestSellingPrice : 7) + (longestStock > 7 ? longestStock : 7) + 8) + '\n'
    }

    R.forEach(item => {
        inventoryString += `|${longestID > 2 ? ' '.repeat(Math.floor(((longestID + 2) - item.id.toString().length) / 2)) : ' '.repeat(Math.floor((4 - item.id.toString().length) / 2))}${item.id}${longestID > 2 ? ' '.repeat(Math.ceil(((longestID + 2) - item.id.toString().length) / 2)) : ' '.repeat(Math.ceil((4 - item.id.toString().length) / 2))}`
        inventoryString += `| ${item.name}${' '.repeat(Math.floor(longestName - item.name.length - 1))}`
        inventoryString += `| ${item.description}${' '.repeat(Math.floor(longestDescription - item.description.length - 1))}`
        if (isManager) {
            inventoryString += `|${longestSellingPrice > 15 ? ' '.repeat(Math.floor((longestSellingPrice - item.sellingPrice.toString().length) / 2)) : ' '.repeat(Math.floor((14 - item.sellingPrice.toString().length) / 2))}${item.sellingPrice}g${longestSellingPrice > 15 ? ' '.repeat(Math.ceil((longestSellingPrice - item.sellingPrice.toString().length) / 2)) : ' '.repeat(Math.ceil((14 - item.sellingPrice.toString().length) / 2))}`
            inventoryString += `|${longestCostPrice > 12 ? ' '.repeat(Math.ceil((longestCostPrice - item.sellingPrice.toString().length) / 2)) : ' '.repeat(Math.ceil((11 - item.costPrice.toString().length) / 2))}${item.costPrice}g${longestCostPrice > 12 ? ' '.repeat(Math.floor((longestCostPrice - item.costPrice.toString().length) / 2)) : ' '.repeat(Math.floor((11 - item.costPrice.toString().length) / 2))}`
            inventoryString += `|${longestStock > 13 ? ' '.repeat(Math.floor((longestStock - item.stock.toString().length) / 2)) : ' '.repeat(Math.floor((13 - item.stock.toString().length) / 2))}${item.stock}${longestStock > 13 ? ' '.repeat(Math.ceil((longestStock - item.stock.toString().length) / 2)) : ' '.repeat(Math.ceil((13 - item.stock.toString().length) / 2))}|`
        }
        else {
            inventoryString += `|${longestSellingPrice > 7 ? ' '.repeat(Math.floor((longestSellingPrice - item.sellingPrice.toString().length) / 2)) : ' '.repeat(Math.floor((6 - item.sellingPrice.toString().length) / 2))}${item.sellingPrice}g${longestSellingPrice > 7 ? ' '.repeat(Math.ceil((longestSellingPrice - item.sellingPrice.toString().length) / 2)) : ' '.repeat(Math.ceil((6 - item.sellingPrice.toString().length) / 2))}`
            inventoryString += `|${longestStock > 7 ? ' '.repeat(Math.floor((longestStock - item.stock.toString().length) / 2)) : ' '.repeat(Math.floor((7 - item.stock.toString().length) / 2))}${item.stock}${longestStock > 7 ? ' '.repeat(Math.ceil((longestStock - item.stock.toString().length) / 2)) : ' '.repeat(Math.ceil((7 - item.stock.toString().length) / 2))}|`
        }
        inventoryString += '\n'
    }, inventory)

    return inventoryString
}

const showCart = cart => {
    let cartString = 'Cart: '
    if (cart.length > 0){
        R.forEach(item => {cartString += `[#${item.id}] ${item.name} - ${item.stock}, `}, cart)
        return cartString.trim().slice(0, cartString.length - 2)
    }
    else {
        return cartString
    }
}

const getTotalOfCart = cart => {
    if (cart.length > 0) {
        const total = R.pipe(
            R.map(item => item.sellingPrice * item.stock),
            R.reduce(R.add, 0)
        )
        return `${total(cart)}g`
    }
    else {
        return '0g'
    }
}

const produceReceipt = (name, date, items, payment, isStore) => {
    let receipt = ''
    receipt += `Date of transaction: ${date}\n${isStore ? `Delivery arrival date: ${new Date(date.setHours(date.getHours() + 24))}` : ''}\nName of customer: ${isStore ? 'Potion Shop™' : name}\nItems purchased:`
    R.forEach(item => {receipt += `\n\t${item.stock}x ${item.name} - ${item.stock * item.sellingPrice}g`}, items)
    receipt += `\nTotal: ${payment}\n`
    return receipt
}

const recordTransaction = (name, date, items, payment, isStore) => {
    if (name === undefined || name === null) {
        name = Maybe.Nothing()
    }
    else {
        name = Maybe.Just(name)
    }
    
    let delivered;
    if (!isStore) {
        delivered = Maybe.Nothing()
    }
    else {
        delivered = Maybe.Just(false)
    }

    let transactions = JSON.parse(fs.readFileSync('./transactions.json', 'utf-8'))
    transactions.push({
        dateOfTransaction: date,
        // deliveryArrivalDate: (isStore ? new Date(date.setHours(date.getHours() + 24)) : null),
        deliveryArrivalDate: (isStore ? new Date(date.setHours(date.getHours() + (1/480))) : null),
        customerName: name.getOrElse('Potion Shop™'),
        delivered: delivered.getOrElse(null),
        itemsBought: items,
        payment: payment
    })
    fs.writeFileSync('./transactions.json', JSON.stringify(transactions, null, 1))
}

const customerInterface = name => {
    let customerName = name
    let customerCart = []

    const determineName = name => {
        return name !== '' ? Maybe.Just(name) : Maybe.Nothing() 
    }
    customerName = determineName(customerName).getOrElse('Anonymous')

    const customerIsAnonymous = value => {
        return determineName(value).matchWith({
            Just: () => false,
            Nothing: () => true
        })
    }

    if (customerIsAnonymous(userInputName)) {
        process.stdout.write('\n"Ah, I see you prefer to keep that information private. Not a problem at all, dear patron!')
    }
    else {
        process.stdout.write(`\n"'${customerName}', a wonderful name!`)
    }
    process.stdout.write(' Now, this way please. And do take your time browsing our wares. I promise you won\'t be dissappointed..."\n\n')

    const isValidCustomerQuery = customerQuery => {
        const validQueries = ['add', 'remove', 'checkout', 'sort']
        const validItemProperties = ['id', 'name', 'description', 'price', 'stock']
        const validSortBy = ['asc', 'desc']
        const keywords = customerQuery.toLowerCase().split(' ')
        const item = R.find(item => item.id === Number(keywords[1]), inventory)

        if (keywords[0] === 'checkout' || ((keywords[0] === 'add' || keywords[0] === 'remove') && keywords[1] === 'all' && keywords[2] === undefined)) {
            return true
        }

        if (!validQueries.includes(keywords[0]) || 
        (keywords[2] === undefined && keywords[0] !== undefined && keywords[1] !== undefined) ||
        (keywords[0] === 'sort' && (!validItemProperties.includes(keywords[1]) || !validSortBy.includes(keywords[2])))) {
            console.log('Invalid query! Please try again.\n')
            return false
        }
        else if (item === undefined && keywords[0] !== 'sort') {
            console.log(`There is no such item with ID '${keywords[1]}' in stock! Please try again.\n`)
            return false
        }
        else if (keywords[0] === 'add' && (Number(keywords[2]) > item.stock || Number(keywords[2]) < 0 || Number(keywords[2] % 1 !== 0))) {
            if (Number(keywords[2]) < 0) {
                console.log('You cannot add a negative amount of items to your cart! Please try again.\n')
            }
            else if (Number(keywords[2]) % 1 !== 0) {
                console.log('You cannot add only part of an item to your cart! Please try again.\n')
            }
            else {
                console.log(`Sorry, but we only have ${item.stock} ${item.name}'s in stock! Please try again.\n`)
            }
            return false
        }
        else if (keywords[0] === 'remove' && (customerCart.findIndex(item => item.id === Number(keywords[1])) === -1 || Number(keywords[2]) < 0 || Number(keywords[2]) % 1 !== 0)) {
            if (Number(keywords[2]) < 0) {
                console.log(`You cannot remove a negative amount of items from your cart! Please try again.\n`)
            }
            else if (Number(keywords[2] % 1 !== 0)) {
                console.log('You cannot remove only a part of an item from your cart! Please try again\n.')
            }
            else {
                console.log(`There is no such item with ID '${keywords[1]}' in your cart! Please try again.\n`)
            }
            return false
        }
        else {
            return true
        }
    }

    const doCustomerQuery = customerQuery => {
        const keywords = customerQuery.toLowerCase().split(' ')
        let inventoryJSON = JSON.parse(fs.readFileSync('./inventory.json', 'utf-8'))
        const findItemIndex = R.findIndex(item => item.id === Number(keywords[1]))
        const sortItemsNumerically = ['id', 'sellingPrice', 'stock']

        if (keywords[0] === 'add') {
            if (keywords[1] === 'all') {
                R.forEach(item => {
                    if (customerCart.findIndex(item2 => item2.id === item.id) === -1) {
                        customerCart.push(item)
                    }
                    else {
                        customerCart[R.findIndex(item2 => item2.id === item.id, customerCart)].stock += item.stock
                    }
                    inventoryJSON[R.indexOf(item, inventory)].stock = 0   
                })(inventory)
            }
            else if (customerCart.find(item => item.id === Number(keywords[1])) === undefined) {
                customerCart.push(R.find(item => item.id === Number(keywords[1]), inventory))
                customerCart[R.findIndex(item => item.id === Number(keywords[1]), customerCart)].stock = Number(keywords[2])   
                inventoryJSON[findItemIndex(inventory)].stock -= Number(keywords[2])
            }
            else {
                customerCart[findItemIndex(customerCart)].stock += Number(keywords[2])
                inventoryJSON[findItemIndex(inventory)].stock -= Number(keywords[2])
            }
        }
        else if (keywords[0] === 'remove') {
            if (keywords[1] === 'all') {
                R.forEach(item => {
                    inventoryJSON[R.findIndex(item2 => item.id === item2.id, inventory)].stock += item.stock
                })(customerCart)
                customerCart = customerCart.slice(0, 0)
            }
            else {
                if (Number(keywords[2]) > customerCart[findItemIndex(customerCart)].stock) {
                    keywords[2] = customerCart[findItemIndex(customerCart)].stock
                }
                customerCart[findItemIndex(customerCart)].stock -= Number(keywords[2])            
                
                if (customerCart[findItemIndex(customerCart)].stock === 0) {
                    customerCart.splice(findItemIndex(customerCart), 1)
                }
                inventoryJSON[findItemIndex(inventory)].stock += Number(keywords[2])
            }
        }
        else if (keywords[0] === 'sort') {
            if (keywords[1] === 'price') {
                keywords[1] = 'sellingPrice'
            }
            
            if (sortItemsNumerically.includes(keywords[1])) {
                if (keywords[2] === 'asc') {
                    inventoryJSON = inventoryJSON.sort((itemA, itemB) => R.prop(keywords[1], itemA) - R.prop(keywords[1], itemB))
                }
                else {
                    inventoryJSON = inventoryJSON.sort((itemA, itemB) => R.prop(keywords[1], itemB) - R.prop(keywords[1], itemA))
                }
            }
            else {
                if (keywords[2] === 'asc') {
                    inventoryJSON = inventoryJSON.sort((itemA, itemB) => R.prop(keywords[1], itemA) > R.prop(keywords[1], itemB))
                }
                else {
                    inventoryJSON = inventoryJSON.sort((itemA, itemB) => R.prop(keywords[1], itemB) > R.prop(keywords[1], itemA))
                }
            }
        }
        fs.writeFileSync('./inventory.json', JSON.stringify(inventoryJSON, null, 1))
        inventory = JSON.parse(fs.readFileSync('./inventory.json', 'utf-8'))
    }

    const customerStartShopping = () => {
        console.log(displayInventory(inventory, false))
        console.log('OPTIONS:\n*Add item to cart - \'add [Item ID] [Amount]\' or \'add all\'\n*Remove item from cart - \'remove [Item ID] [Amount]\' or \'remove all\'\n*Sort by - \'sort [id/name/description/price/stock] [asc/desc]\'\n*Check-out - \'checkout\'\n')
        console.log(showCart(customerCart))
        console.log(`Total: ${getTotalOfCart(customerCart)}`)

        const customerQuery = readline.question('[Enter query here]: ')

        console.clear()
        if (!isValidCustomerQuery(customerQuery)) {
            customerStartShopping()
        }
        else {
            doCustomerQuery(customerQuery)
            if (customerQuery.toLowerCase() !== 'checkout') {
                console.log('"Very well, dear patron! Anything else?"\n')
                customerStartShopping()
            }
            else {
                console.log('"Thank you for your kind patronage, dear customer.')
                if (customerCart.length > 0) {
                    recordTransaction(customerName, new Date(), customerCart, getTotalOfCart(customerCart), false)
                    console.log('\nHere is your receipt:')
                    console.log(produceReceipt(customerName, new Date(), customerCart, getTotalOfCart(customerCart), false))
                }
                console.log('We hope to you see you again soon..."')
            }
        }
        return 0
    }
    customerStartShopping()
}

const managerInterface = () => {
    let managerCart = []

    const isValidManagerQuery = managerQuery => {
        const validQueries = ['add', 'remove', 'throw', 'price', 'sort', 'order', 'logout']
        const validItemProperties = ['id', 'name', 'description', 'sprice', 'cprice', 'stock']
        const validSortBy = ['asc', 'desc']
        const keywords = managerQuery.toLowerCase().split(' ')
        const item = R.find(item => item.id === Number(keywords[1]), inventory)

        if ((keywords[0] === 'order' && managerCart.length > 0) || keywords[0] === 'logout' || ((keywords[0] === 'remove' || keywords[0] === 'throw') && keywords[1] === 'all' && keywords[2] === undefined)) {
            return true
        }

        if (!validQueries.includes(keywords[0]) ||
        (keywords[0] !== undefined && keywords[1] === undefined) || 
        (keywords[2] === undefined && keywords[0] !== undefined && keywords[1] !== undefined) ||
        (keywords[0] === 'sort' && (!validItemProperties.includes(keywords[1]) || !validSortBy.includes(keywords[2])))) {
            console.log('Invalid query! Please try again.\n')
            return false
        }
        else if (keywords[0] === 'order' && managerCart.length === 0) {
            console.log('You have not placed any items into your cart yet! Please try again.\n')
            return false
        }
        else if (item === undefined && keywords[0] !== 'sort') {
            if (keywords[0] === 'add') {
                console.log(`There is no such item with ID '${keywords[1]}' for order! Please try again.\n`)
            }
            else if (keywords[0] === 'price') {
                console.log(`There is no such item with ID ${keywords[1]} in the inventory! Please try again.\n`)
            }
            return false
        }
        else if (keywords[0] === 'add' && (Number(keywords[2]) < 0 || Number(keywords[2]) % 1 !== 0)) {
            if (Number(keywords[2] < 0)) {
                console.log('You cannot add a negative amount of items to your cart! Please try again.\n')
            }
            else {
                console.log('You cannot add only a part of an item to your cart! Please try again.\n')
            }
            return false
        }
        else if (keywords[0] === 'remove' && (managerCart.findIndex(item => item.id === Number(keywords[1])) === -1 || Number(keywords[2]) < 0 || Number(keywords[2]) % 1 !== 0)) {
            if (Number(keywords[2]) < 0) {
            console.log(`You cannot remove a negative amount of items from your cart! Please try again.\n`)
            }
            else if (Number(keywords[2]) % 1 !== 0) {
                console.log(`You cannot remove only a part of an object from your cart! Please try again.\n`)
            }
            else {
                console.log(`There is no such item with ID '${keywords[1]}' in your cart! Please try again.\n`)
            }
            return false
        }
        else if (keywords[0] === 'price' && keywords[2] < 0) {
            console.log('You cannot set an item\'s price to below 0! Please try again.')
            return false
        }
        else {
            return true
        }
    }

    const doManagerQuery = managerQuery => {
        const keywords = managerQuery.toLowerCase().split(' ')
        let inventoryJSON = JSON.parse(fs.readFileSync('./inventory.json', 'utf-8'))
        const findItemIndex = R.findIndex(item => item.id === Number(keywords[1]))
        const sortItemsNumerically = ['id', 'sellingPrice', 'costPrice', 'stock']

        if (keywords[0] === 'add') {
            if (managerCart.find(item => item.id === Number(keywords[1])) === undefined) {
                managerCart.push(R.find(item => item.id === Number(keywords[1]), inventory))
                managerCart[R.findIndex(item => item.id === Number(keywords[1]), managerCart)].stock = Number(keywords[2])   
            }
            else {
                managerCart[findItemIndex(managerCart)].stock += Number(keywords[2])
            }
        }
        else if (keywords[0] === 'remove') {
            if (keywords[1] === 'all') {
                managerCart = managerCart.slice(0, 0)
            }
            else {
                if (Number(keywords[2]) > managerCart[findItemIndex(managerCart)].stock) {
                    keywords[2] = managerCart[findItemIndex(managerCart)].stock
                }
                managerCart[findItemIndex(managerCart)].stock -= Number(keywords[2])            

                if (managerCart[findItemIndex(managerCart)].stock === 0) {
                    managerCart.splice(findItemIndex(managerCart), 1)
                }
            }
        }
        else if (keywords[0] === 'throw') {
            if (keywords[1] === 'all') {
                R.forEach(item => item.stock = 0, inventoryJSON)
            }
            else {
                if (keywords[2] > inventoryJSON[findItemIndex(inventory)].stock) {
                    keywords[2] = inventoryJSON[findItemIndex(inventory)].stock
                }
                inventoryJSON[findItemIndex(inventory)].stock -= Number(keywords[2])
            }
        }
        else if (keywords[0] === 'price') {
            inventoryJSON[findItemIndex(inventory)].sellingPrice = Number(keywords[2])
        }
        else if (keywords[0] === 'sort') {
            if (keywords[1] === 'sprice') {
                keywords[1] = 'sellingPrice'
            }
            else if (keywords[1] === 'cprice') {
                keywords[1] = 'costPrice'
            }
            
            if (sortItemsNumerically.includes(keywords[1])) {
                if (keywords[2] === 'asc') {
                    inventoryJSON = inventoryJSON.sort((itemA, itemB) => R.prop(keywords[1], itemA) - R.prop(keywords[1], itemB))
                }
                else {
                    inventoryJSON = inventoryJSON.sort((itemA, itemB) => R.prop(keywords[1], itemB) - R.prop(keywords[1], itemA))
                }
            }
            else {
                if (keywords[2] === 'asc') {
                    inventoryJSON = inventoryJSON.sort((itemA, itemB) => R.prop(keywords[1], itemA) > R.prop(keywords[1], itemB))
                }
                else {
                    inventoryJSON = inventoryJSON.sort((itemA, itemB) => R.prop(keywords[1], itemB) > R.prop(keywords[1], itemA))
                }
            }
        }
        else if (keywords[0] === 'order') {
            console.log('Your order has been placed and will arrive after 24 hours! (but for the sake of testing it will actually arrive instantly)')
            console.log('\nHere is your receipt:')
            console.log(produceReceipt(null, new Date(), managerCart, getTotalOfCart(managerCart), true))
            recordTransaction(null, new Date(), managerCart, getTotalOfCart(managerCart), true)
            console.log('Thank you and we hope you order again!\n')
            managerCart = []
        }

        fs.writeFileSync('./inventory.json', JSON.stringify(inventoryJSON, null, 1))
        inventory = JSON.parse(fs.readFileSync('./inventory.json', 'utf-8'))
    }

    const managerStartManaging = () => {
        console.log(displayInventory(inventory, true))
        console.log('OPTIONS:\n*Add item to cart for stock order - \'add [Item ID] [Amount]\'\n*Remove item from cart for stock order - \'remove [Item ID] [Amount]\' or \'remove all\'\n*Throw away item stocks - \'throw [Item ID] [Amount]\' or \'throw all\'\nSet item price - \'price [Item ID] [New Price]\'\n*Sort by - \'sort [id/name/description/sPrice/cPrice/stock] [asc/desc]\'\n*Finish order and check-out cart - \'order\'\n*Log-out - \'logout\'\n')
        console.log(showCart(managerCart))
        console.log(`Total: ${getTotalOfCart(managerCart)}`)

        const managerQuery = readline.question('[Enter query here]: ')

        console.clear()
        if (!isValidManagerQuery(managerQuery)) {
            managerStartManaging()
        }
        else {
            doManagerQuery(managerQuery)
            if (managerQuery.toLowerCase() !== 'logout') {
                console.log('"Very well, dear manager! Anything else?"\n')
                managerStartManaging()
            }
            else {
                console.log('"Thank you for your service, dear manager.')
                console.log('Until next time..."')
            }
        }
        return 0
    }

    managerStartManaging()
}

const welcomeSign = '~~~~~ Welcome to the Potion Shop™ ~~~~~'
console.log('±'.repeat(welcomeSign.length))
console.log(welcomeSign)
console.log('±'.repeat(welcomeSign.length) + '\n')

const timeOfDay = new Date().getHours()
// const timeOfDay = 3
const openingTime = 7
const closingTime = 22

if (timeOfDay < openingTime || timeOfDay >= closingTime) {
    console.log(`Our apologies, dear patron, but we are closed at the moment. Kindly come back tomorrow at around ${openingTime} in the morning.`)
}
else if (timeOfDay >= 17) {
    process.stdout.write('"Good evening')
}
else if (timeOfDay >= 12) {
    process.stdout.write('"Good afternoon')
}
else {
    process.stdout.write('"Good morrow')
}

let userInputName
if (timeOfDay > openingTime && timeOfDay < closingTime) {
    process.stdout.write(', dear patron! And who might it be that we have the pleasure of serving today?"\n')
    userInputName = readline.question('[Enter name]: ')
}
else {
    userInputName = readline.question('')
}

if (userInputName.toLowerCase().replace(/[?!.,]/g, '') === 'i am the one who knocks') {
    if (timeOfDay < openingTime || timeOfDay >= closingTime) {
        console.log('\n"Ah! My sincerest apologies, dear manager, I had not known it was you. The darkness of the night impairs my ability to discern figures. Well, no matter!')
    }
    else {
        console.log('\n"Welcome back, dear manager! I do hope I have kept the shop as well-maintained to your liking in your absence, but I think there are no doubts about that...')
    }

    console.log('Come inside now. And do take your time. I know yours is a very important job...') 
    if (newDeliveriesArrived) {
        console.log('\nOh, and by the by, new stocks have arrived! Here are their receipts:\n')
        R.forEach(delivery => console.log(produceReceipt(delivery.customerName, new Date(delivery.dateOfTransaction), delivery.itemsBought, delivery.payment, true)), newDeliveries)
        console.log('\nThat is all. You may continue with your duties now, dear manager.')
    }
    managerInterface()
}
else {
    customerInterface(userInputName)
}

inventory = inventory.sort((itemA, itemB) => R.prop('id', itemA) > R.prop('id', itemB))
fs.writeFileSync('./inventory.json', JSON.stringify(inventory, null, 2))