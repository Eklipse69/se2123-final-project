const readline = require('readline-sync')
const R = require('ramda')
const fs = require('fs')
const Maybe = require('folktale/maybe')
let inventory = JSON.parse(fs.readFileSync('./inventory.json', 'utf-8'))
const transactions = JSON.parse(fs.readFileSync('./transactions.json', 'utf-8'))

// const checkDueDeliveries = R.pipe(
//     R.map(transaction => ({...transaction, deliveryArrivalDate: new Date(transaction.deliveryArrivalDate)})),
//     R.filter(R.converge(R.equals(false), [R.prop('delivered')])),
//     R.filter(R.converge(R.gt(new Date()), [R.prop('deliveryArrivalDate')]))
// )

processDueDeliveries()